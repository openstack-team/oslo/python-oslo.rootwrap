python-oslo.rootwrap (7.5.1-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 16:34:30 +0100

python-oslo.rootwrap (7.5.1-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-debtcollector (>= 3.0.0) as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 17:30:06 +0100

python-oslo.rootwrap (7.3.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090577).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 08:33:05 +0100

python-oslo.rootwrap (7.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:58:55 +0200

python-oslo.rootwrap (7.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 17:18:42 +0200

python-oslo.rootwrap (7.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:43:00 +0200

python-oslo.rootwrap (7.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 22:40:08 +0100

python-oslo.rootwrap (7.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 14:02:10 +0200

python-oslo.rootwrap (7.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 14:59:30 +0200

python-oslo.rootwrap (7.0.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1046816).

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Aug 2023 10:42:43 +0200

python-oslo.rootwrap (7.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:42:26 +0200

python-oslo.rootwrap (7.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 11:20:24 +0100

python-oslo.rootwrap (6.3.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 09:00:36 +0100

python-oslo.rootwrap (6.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Feb 2022 09:28:36 +0100

python-oslo.rootwrap (6.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 16:55:36 +0200

python-oslo.rootwrap (6.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 15:35:49 +0200

python-oslo.rootwrap (6.2.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 10:06:14 +0200

python-oslo.rootwrap (6.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:39:56 +0200

python-oslo.rootwrap (6.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 21:33:06 +0200

python-oslo.rootwrap (6.0.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 22:20:11 +0200

python-oslo.rootwrap (6.0.2-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-mock from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 15:46:51 +0200

python-oslo.rootwrap (5.16.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:46:55 +0200

python-oslo.rootwrap (5.16.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Sep 2019 09:30:36 +0200

python-oslo.rootwrap (5.16.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Sep 2019 14:19:27 +0200

python-oslo.rootwrap (5.15.2-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 01:12:51 +0200

python-oslo.rootwrap (5.15.2-2) experimental; urgency=medium

  * Do not install alternatives.

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Mar 2019 22:42:19 +0100

python-oslo.rootwrap (5.15.2-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Removed do-not-run-toplevel-tests.patch, not revelant anymore since the
    move to stestr.
  * Do not run functional tests.

 -- Thomas Goirand <zigo@debian.org>  Fri, 22 Mar 2019 09:44:34 +0100

python-oslo.rootwrap (5.14.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 23:16:44 +0200

python-oslo.rootwrap (5.14.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Aug 2018 11:36:52 +0200

python-oslo.rootwrap (5.13.0-3) unstable; urgency=medium

  * Fixed Python 3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Mar 2018 18:55:28 +0000

python-oslo.rootwrap (5.13.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:27:00 +0000

python-oslo.rootwrap (5.13.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Remove python-3.6-fix.patch, applied upstream.
  * Standards-Version is now 4.1.3.
  * Using debhelper 10.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 12:41:13 +0000

python-oslo.rootwrap (5.9.0-2) unstable; urgency=medium

  * Uploading to unstable:
    - Fix FTBFS with Python 3.6 (Closes: #867632).

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Oct 2017 17:03:19 +0000

python-oslo.rootwrap (5.9.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed now useless transition packages.
  * Add patch for Python 3.6.
  * Using pkgos-dh_auto_install.

  [ Daniel Baumann ]
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Aug 2017 00:46:29 +0000

python-oslo.rootwrap (4.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 10:15:45 +0000

python-oslo.rootwrap (4.1.0-1) experimental; urgency=medium

  [ Corey Bryant ]
  * New upstream release.

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Standards-Version: 3.9.7 (no change).
  * Fixed (build-)depends for this release.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 02:16:02 +0000

python-oslo.rootwrap (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 08:52:04 +0000

python-oslo.rootwrap (3.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 11:49:33 +0100

python-oslo.rootwrap (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 20:33:43 +0000

python-oslo.rootwrap (2.3.0-1) experimental; urgency=medium

  [ James Page ]
  * Update Vcs fields for new git repository locations.
  * Fix typo in transitional package descriptions (LP: #1471561).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Do not a run test failing in Python 3.4:
    test_rootwrap.RootwrapTestCase.test_KillFilter_renamed_exe
  * python-oslo-rootwrap (transition package) in priority extra.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 18:45:12 +0200

python-oslo.rootwrap (2.0.0-2) experimental; urgency=medium

  * Using update-alternatives to handle /usr/bin/oslo-rootwrap using either
    Python 2 or 3 (Closes: #788847).
  * Updated debian/watch file to use github tags instead of PyPi.
  * Updated Homepage: in d/rules and Source: in d/copyright.

 -- Thomas Goirand <zigo@debian.org>  Mon, 15 Jun 2015 14:59:45 +0000

python-oslo.rootwrap (2.0.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align dependency version requirements with upstream.
  * Rename package to python-oslo.rootwrap.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces and transitional package for
      python-oslo-rootwrap in Ubuntu.

 -- James Page <jamespage@debian.org>  Wed, 10 Jun 2015 11:21:07 +0100

oslo.rootwrap (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Feb 2015 12:03:20 +0100

oslo.rootwrap (1.3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Oct 2014 12:22:40 +0000

oslo.rootwrap (1.3.0.0~a3-1) experimental; urgency=medium

  * New upstream release.
  * Uploading to experimental just right before the Jessie freeze.
  * Fixed (build-)depends.
  * Do not run the unit tests using Python 2.6.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Sep 2014 00:14:28 +0800

oslo.rootwrap (1.2.0-2) unstable; urgency=medium

  * Fixed wrong VCS URLs.
  * Added Python 3 support.
  * Runs unit tests on build, build-conflicts with other python-osolo.*
    packages which are on the same namespace, and then preventing from doing
    "import oslo.rootwrap" from the current dir.

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 Aug 2014 14:45:45 +0000

oslo.rootwrap (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Using testr / subunit correctly.
  * Removed Debian patch (applied upstream).
  * Now depends on sudo (Closes: #746501). Thanks to Benedikt Trefzer
    <benedikt.trefzer@cirrax.com> for the bug report.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 May 2014 16:35:22 +0800

oslo.rootwrap (1.0.0-2) unstable; urgency=medium

  * Fixes unit tests by cherry-picking a commit upstream.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Jan 2014 14:30:17 +0000

oslo.rootwrap (1.0.0-1) unstable; urgency=low

  * Initial release. (Closes: #736485)

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Dec 2013 15:46:09 +0800
